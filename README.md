<br>
<h1 align="center"> Movie/Tv Organiser</h1>


<h4> This project has not undergone regression test, so use on your own will </h4>

<h2> What does it do? </h2>
<b>Before : </b><br>
![before](/uploads/01df1d861dfc787fbcae754fd8b7c136/before.PNG)
<br><b>After : </b><br>
![after](/uploads/3a4e5719c37951176c67a04589e53981/after.PNG)
<br>

<h3>Requirements</h3>
<ol>
<li>Name the folders and files with correct movie/tvshow names (if not named properly).</li>
<li>If possible try to move other files to seperate directory so that it doesn't get affected.</li>
</ol>

<br>

<b>Installing Pip</b>

If you have Python 2 >=2.7.9 or Python 3 >=3.4 installed from python.org, you will already have pip and setuptools, but will need to upgrade to the latest version:<br>
You'll find pip executable in PATH_TO_PYTHON/Scripts

On Linux or OS X:

<code>pip install -U pip setuptools</code>

On Windows:

<code>python -m pip install -U pip setuptools</code>

<br>
<b>Installing Guessit</b>

install Guessit python plugin : <code>pip install guessit</code>

<br>
<h3>How to run?</h3>
run : <code>python arrangeMovies.py  <i>to_be_arranged_directory</i></code>

eg:

<code>python arrangeMovies.py ./TestFolders</code> (copy all the folders from <b>backuptestfolder</b> into <b>TestFolders</b> before running this)<br>
<code>python arrangeMovies.py home/path_to_downloads/Downloads</code> (to arrange the downloads)

<br>
<h5>How long should i wait before this process completes?</h5>
Each file takes a max of 1.1s and you can do the math.<br>

<br>
<b>Hacks</b><br>
You can add this as a code to your right-click menu using other third party softwares

<br>
<b>Running it as Service</b><br>
<i>Under progress</i>



