import os
import sys
import re
import urllib
from guessit import guessit
import time
import json
import shutil
import io

titleUrl = "http://www.omdbapi.com/?plot=short&r=json&t="

def getDirectoryList(path):
    return os.listdir(path)

def getMovieDetailFromUrl(movieName,year):
    print movieName
    urlMovieName = movieName.strip().replace(' ','+')
    url = titleUrl+urlMovieName
    if year:
        print year
        url = url + "&y="+str(year)
    omdbResult = urllib.urlopen(url)
    time.sleep(1)
    return json.loads(omdbResult.read())

def getMovieDetail(movieNames,path):
    languageMovieDict = {}
    for movieName in movieNames:
        if not movieName[1].has_key('title'):
            continue;
        year = None
        if  movieName[1].has_key("year"):
            year = movieName[1]['year']
        details = getMovieDetailFromUrl(movieName[1]['title'],year)
        if details['Response'] == 'True':
            addToDict(languageMovieDict,details,movieName[0])
    printWithPath(languageMovieDict,path)

def printWithPath(dict,oldpath):
    for key,vals in dict.items():
        newPath = oldpath+"/"+key+"/";
        for val in vals:
            try:
                moveDir(oldpath, newPath, val[0],val[1])
            except ValueError:
                print "OOps"

def createPrintInfo(details):
    str = ""
    for key,item in details.items():
        str += "%s : %s" %(key,item)
        str  += "\n"
    return str

def writeInfoFile(path,details,info):
    infoPath = os.path.join(path,details['Title']+".info")
    with io.FileIO(infoPath, "w") as file:
        file.write(info)

def createTargetPath(dirPath,src):
    for dir in dirPath:
        newSrc = os.path.join(src,dir)
        if not os.path.exists(newSrc):
            os.makedirs(newSrc)
        src = newSrc
    return src

def moveDir(src,target,folder,details):
    language = details['Language'].split(',')[0]
    dirPath = [language,details['Type']]
    targetFileName = str(details['imdbRating'])+"_"+details['Title']
    if not os.path.isdir(os.path.join(src,folder)):
        dirPath.append(targetFileName)
    target = createTargetPath(dirPath,src)
    src = os.path.join(src,folder)
    info  = createPrintInfo(details)

    # if os.path.isdir(src):
    #     target = os.path.join(target,folder)
    # else:
    #     print "Making path"
    #     if not os.path.exists(os.path.join(target,details['Title'])):
    #         os.makedirs(os.path.join(target,details['Title']))
    #     target = os.path.join(os.path.join(target,details['Title']),folder)


    if os.path.isdir(src):
        folder = targetFileName
    target = os.path.join(target,folder)
    print "Copying from %s -> %s" %(src,target)
    if os.path.exists(target):
        return

    try:
        shutil.move(src, target)
        print "Writing Info"
        writeInfoFile(target,details,info)
    except ValueError:
        print "Oops"

def addToDict(dict,details,filename):
    language = details['Language'].split(',')[0]
    genre = "+".join(str(x) for x in details['Genre'].split(','))
    if dict.has_key(language):
        dict[language].append((filename,details))
    else:
        dict[language] = [(filename,details)]

def getMovieName(filename):
    guess =  guessit(filename)

    if guess:
        return (filename,guess)

def getMovieNamesFromDirectory(path):
    names = getDirectoryList(path)
    movieNames = [getMovieName(name) for name in names ]
    getMovieDetail(movieNames,path)

def cleanFolder():
    getMovieNamesFromDirectory('/home/naveen/Documents/Share')

# if __name__ == "__main__":
#     main()