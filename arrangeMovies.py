import os
import sys
import re
import urllib
from guessit import guessit
import time
import json
import shutil
import io

titleUrl = "http://www.omdbapi.com/?plot=short&r=json&t="

def getDirectoryList(path):
    return os.listdir(path)

def getMovieDetailFromUrl(movieName,year):
    """
		Call omdb api and returns details as an object
	"""
    urlMovieName = movieName.strip().replace(' ','+')
    url = titleUrl+urlMovieName
    if year:
        print year
        url = url + "&y="+str(year)
    omdbResult = urllib.urlopen(url)
    time.sleep(1)
    return json.loads(omdbResult.read())

def getMovieDetail(movieNames,path):
    languageMovieDict = {}
    for movieName in movieNames:
		#check whether guessit has guessed the title
        if not movieName[1].has_key('title'):
            continue;
        else:
            year,title = None,movieName[1]['title']
            if  movieName[1].has_key("year"):
                year = movieName[1]['year']

            if languageMovieDict.has_key(title):
                addToDict(languageMovieDict,languageMovieDict[title][0][1],movieName[0],title,movieName[1])
            else:
                details = getMovieDetailFromUrl(title,year)
                if details['Response'] == 'True':
                    addToDict(languageMovieDict,details,movieName[0],title,movieName[1])

    printWithPath(languageMovieDict,path)

def printWithPath(dict,oldpath):
    for key,vals in dict.items():
        for val in vals:
			# val is (filename,details,guessedDetails)
            try:
                moveDir(oldpath, val[0],val[1], val[2])
            except ValueError:
                print "Value error"


def createPrintInfo(details):
    str = ""
    for key,item in details.items():
        str += "%s : %s" %(key,item)
        str  += "\n"
    return str

def writeInfoFile(path,details,info):
    infoPath = os.path.join(path,details['Title']+" ("+details['Genre']+")"+".info")
    with io.FileIO(infoPath, "w") as file:
        file.write(info.encode(encoding='UTF-8',errors='ignore'))

def createTargetPath(dirPath,src):
    for dir in dirPath:
        newSrc = os.path.join(src,dir)
        if not os.path.exists(newSrc):
            os.makedirs(newSrc)
        src = newSrc
    return src

def moveDir(src,filename,details, guessedDetails):
    language = details['Language'].split(',')[0]
    rating = str(details['imdbRating'])
    if len(language) < 4:
        language = "English"
    dirPath = [language,details['Type']]
    if details['Type'] == 'series' and guessedDetails.has_key('season'):
        dirPath.append(rating+"_"+details['Title'])
        dirPath.append("S-"+str(guessedDetails['season']))
    targetFileName = details['Title']
    if not os.path.isdir(os.path.join(src,filename)):
        dirPath.append(rating+"_"+targetFileName)
    target = createTargetPath(dirPath,src)
    src = os.path.join(src,filename)
    info  = createPrintInfo(details)

    if not os.path.isdir(src):
        writeInfoFile(target,details,info)
    target = os.path.join(target,filename)
    print "Copying from %s -> %s" %(src,target)
    if os.path.exists(target):
        return

    try:
        shutil.move(src, target)
        if os.path.isdir(target):
            writeInfoFile(target,details,info)
            if details['Type'] != 'series':
                newPathWithRating = rating+"_"+os.path.basename(target)
                newTarget = target.replace(os.path.basename(target), newPathWithRating)
                print "Renaming",target,"to",newTarget
                os.rename(target,newTarget)
        print "Writing Info"

    except ValueError:
        print ValueError

def addToDict(dict,details,filename,title,guessedDetails):
    if dict.has_key(title):
        dict[title].append((filename,details,guessedDetails))
    else:
        dict[title] = [(filename,details,guessedDetails)]

def preprocess(fname):
    fname = re.sub("www\.\S+\.\w{2,3}",'',fname)
    fname = re.sub('[.-]',' ',fname)
    fname = fname.replace("axxo","")
    print "Processing",fname
    return fname

def getMovieName(filename):
    processedfilename = preprocess(filename)
    guess =  guessit(processedfilename)

    if guess:
        return (filename,guess)

def getMovieNamesFromDirectory(path):
    """
	Returns a list of tuple of (filename, Guessit object)
	"""
    names = getDirectoryList(path)
    movieNames = [getMovieName(name) for name in names ]
    return movieNames

def main():
    path = sys.argv[1]
    movieNames = getMovieNamesFromDirectory(path)
    getMovieDetail(movieNames,path)

if __name__ == "__main__":
    main()